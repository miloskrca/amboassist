package rs.etf.km123247.amboassist;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Krca on 9/11/13.
 */
class RetreiveAllPatientsTask extends AsyncTask<String, Void, String> {

    private final String baseUrl;
    private final String destinationUrl;
    private ProgressDialog mDialog;

    private ListView view;

    public RetreiveAllPatientsTask(ListView textView, String baseUrl, String destinationUrl) {
        this.view = textView;
        this.baseUrl = baseUrl;
        this.destinationUrl = destinationUrl;
    }

    protected void onPreExecute() {
        try {
            if (this.mDialog == null) {
                this.mDialog = new ProgressDialog(this.view.getContext());
                this.mDialog.setMessage("Loading...");
                this.mDialog.setCancelable(false);
            }
            this.mDialog.show();
        } catch (Exception ignored) {

        }
    }

    protected String doInBackground(String... urls) {
        return HttpClient.SendHttpGet(this.baseUrl + this.destinationUrl);
    }

    protected void onPostExecute(String jsonString) {
        boolean errorOccurred = false;
        String errorString = jsonString;
        try {
            if (jsonString == null) {
                throw new Exception();
            }

            JSONArray jsons;
            jsons = new JSONArray(jsonString);
            List<Patient> patients = new ArrayList<Patient>();
            for (int i = 0; i < jsons.length(); i++) {
                JSONObject object = ((JSONObject) jsons.get(i));

                Patient patient = new Patient((String) object.get("name"));
                patient.setId((Integer) object.get("id"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date d = dateFormat.parse((String) object.get("dob"));

                patient.setDob(d);
                patient.setJmbg((String) object.get("jmbg"));
                patient.setSex((Boolean) object.get("sex"));
                patients.add(patient);

                Patient.patientList.add(patient);
            }
            PatientItemAdapter adapter = new PatientItemAdapter(view.getContext(),
                    R.layout.patients_list_item_layout, patients.toArray(new Patient[patients.size()]));

            view.setAdapter(adapter);
        } catch (Exception e) {
            errorOccurred = true;
            errorString = "jsonString = null";
            if(e.getMessage() != null) {
                errorString = e.getMessage();
                Log.i("ERROR", e.getMessage());
            }
        } finally {
            if (errorOccurred) {

                AlertDialog alertDialog = new AlertDialog.Builder(
                        view.getContext()).create();
                // Setting Dialog Title
                alertDialog.setTitle("Error Patients");
                // Setting Dialog Message
                alertDialog.setMessage("Connection couldn't be established " + errorString);
                // Setting OK Button
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }

            this.mDialog.dismiss();
        }
    }
}