package rs.etf.km123247.amboassist;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    public static MainActivity mActivity = null;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    public static String BASE_URL = "http://amboassist-35792.euw1.actionbox.io:3000/";
    public static int CONNECTION_TIMEOUT_SEC = 5;
    public static int SOCKET_TIMEOUT_SEC = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

        mActivity = this;
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a SectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = new SectionFragment();
            Bundle args = new Bundle();
            args.putInt(SectionFragment.ARG_SECTION_NUMBER, position + 1);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    public static void alert(Context c, String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setMessage(s)
                .setCancelable(false)
                .setPositiveButton(c.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void alertSinglePatientFound(Context c) {

        Patient patient = Patient.foundPatient;
        if (patient == null) {
            return;
        }
        String gender = patient.getSex() ? getString(R.string.patient_male) : getString(R.string.patient_female);
        String msg = getString(R.string.alert_title_visits_get_patient) +
                "\n\n" + getString(R.string.patient_name) + "\n" + patient.getName() +
                "\n" + getString(R.string.visit_patient_jmbg) + ":\n" + patient.getJmbg() +
                "\n" + getString(R.string.patient_gender) + ":\n" + gender +
                "\n" + getString(R.string.patient_dob) + ":\n" + patient.getDobString() +
                "\n\n" + getString(R.string.alert_title_visits_get_patient_question);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setCancelable(true)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mViewPager.setCurrentItem(1);
                        View rootView = mViewPager.getChildAt(1).getRootView();
                        Patient patient = Patient.foundPatient;
                        if (patient == null) {
                            return;
                        }

                        EditText nameView = (EditText) rootView.findViewById(R.id.patient_name);
                        EditText jmbgView = (EditText) rootView.findViewById(R.id.patient_jmbg);
                        DatePicker dobView = (DatePicker) rootView.findViewById(R.id.patient_dob);
                        RadioButton maleView = (RadioButton) rootView.findViewById(R.id.patient_male);
                        RadioButton femaleView = (RadioButton) rootView.findViewById(R.id.patient_female);

                        PatientTextWatcher.flags.put(PatientTextWatcher.NAME, false);
                        PatientTextWatcher.flags.put(PatientTextWatcher.JMBG, false);
                        PatientTextWatcher.flags.put(PatientTextWatcher.SEX, false);

                        nameView.setText(patient.getName());
                        jmbgView.setText(patient.getJmbg());
                        if (patient.getSex()) maleView.setChecked(true);
                        else femaleView.setChecked(true);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(patient.getDob());
                        dobView.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                        PatientTextWatcher.flags.put(PatientTextWatcher.NAME, true);
                        PatientTextWatcher.flags.put(PatientTextWatcher.JMBG, true);
                        PatientTextWatcher.flags.put(PatientTextWatcher.SEX, true);
                    }
                }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (SectionFragment.VISIT_FROM_VISIT): {
                if (resultCode == Activity.RESULT_OK) {
                    Patient patient = (Patient) data.getSerializableExtra("patient");
                    mViewPager.setCurrentItem(1);
                    View rootView = mViewPager.getChildAt(1).getRootView();
                    assert patient != null;

                    EditText nameView = (EditText) rootView.findViewById(R.id.patient_name);
                    EditText jmbgView = (EditText) rootView.findViewById(R.id.patient_jmbg);
                    DatePicker dobView = (DatePicker) rootView.findViewById(R.id.patient_dob);
                    RadioButton maleView = (RadioButton) rootView.findViewById(R.id.patient_male);
                    RadioButton femaleView = (RadioButton) rootView.findViewById(R.id.patient_female);

                    PatientTextWatcher.flags.put(PatientTextWatcher.NAME, false);
                    PatientTextWatcher.flags.put(PatientTextWatcher.JMBG, false);
                    PatientTextWatcher.flags.put(PatientTextWatcher.SEX, false);

                    nameView.setText(patient.getName());
                    jmbgView.setText(patient.getJmbg());
                    if (patient.getSex()) maleView.setChecked(true);
                    else femaleView.setChecked(true);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(patient.getDob());
                    dobView.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                    PatientTextWatcher.flags.put(PatientTextWatcher.NAME, true);
                    PatientTextWatcher.flags.put(PatientTextWatcher.JMBG, true);
                    PatientTextWatcher.flags.put(PatientTextWatcher.SEX, true);
                }
                break;
            }
        }
    }
}
