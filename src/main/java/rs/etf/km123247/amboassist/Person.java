package rs.etf.km123247.amboassist;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Krca on 9/11/13.
 */
public class Person implements Serializable {
    private Integer id;
    private String name;
    private Date dob;
    private String jmbg;
    private boolean sex; // 1 - male, 0 - female

    public static boolean MALE = true;
    public static boolean FEMALE = false;

    public Person(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public boolean getSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getDobString() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.getDob());
        String dobString = calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR);

        return dobString;
    }
}
