package rs.etf.km123247.amboassist;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Krca on 9/15/13.
*/
public class SettingsActivity extends Activity {
    public static String SETTINGS_URL = "settings_url";
    public static String SETTINGS_GET_PATIENT = "settings_get_patient";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}