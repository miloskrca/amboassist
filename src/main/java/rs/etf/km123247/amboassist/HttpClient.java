package rs.etf.km123247.amboassist;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;

public class HttpClient {
    public static String    SendHttpGet(String URL) {
        String responseString;
        HttpResponse response;
        try {
            DefaultHttpClient client = new DefaultHttpClient();

            final HttpParams httpParameters = client.getParams();

            // Set the timeout in milliseconds until a connection is established.
            // The default value is zero, that means the timeout is not used.
            HttpConnectionParams.setConnectionTimeout(httpParameters, MainActivity.CONNECTION_TIMEOUT_SEC * 5000);

            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            HttpConnectionParams.setSoTimeout(httpParameters, MainActivity.SOCKET_TIMEOUT_SEC * 5000);

            HttpGet request = new HttpGet();
            request.setURI(new URI(URL.replace(" ", "%20")));
            response = client.execute(request);
            responseString = convertStreamToString(response.getEntity().getContent());

        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            responseString = null;
        } catch (Exception e) {
            e.printStackTrace();
            responseString = null;
        }

        return responseString;
    }

    public static String SendHttpPost(String URL, List<NameValuePair> nameValuePairs) {
        String responseCode;
        HttpResponse response;
        try {
            DefaultHttpClient client = new DefaultHttpClient();

            final HttpParams httpParameters = client.getParams();

            // Set the timeout in milliseconds until a connection is established.
            // The default value is zero, that means the timeout is not used.
            HttpConnectionParams.setConnectionTimeout(httpParameters, MainActivity.CONNECTION_TIMEOUT_SEC * 2000);

            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            HttpConnectionParams.setSoTimeout(httpParameters, MainActivity.SOCKET_TIMEOUT_SEC * 2000);

            HttpPost request = new HttpPost(URL);
            request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            response = client.execute(request);
            responseCode = response.getStatusLine().getStatusCode() + "";

        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            responseCode = null;
        } catch (Exception e) {
            e.printStackTrace();
            responseCode = null;
        }

        return responseCode;
    }

    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 *
		 * (c) public domain: http://senior.ceng.metu.edu.tr/2009/praeda/2009/01/11/a-simple-restful-client-at-android/
		 */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String SendHttpGetPatient(String URL, List<NameValuePair> namePairValues) {
        String responseString;
        HttpResponse response;
        try {
            DefaultHttpClient client = new DefaultHttpClient();

            final HttpParams httpParameters = client.getParams();

            // Set the timeout in milliseconds until a connection is established.
            // The default value is zero, that means the timeout is not used.
            HttpConnectionParams.setConnectionTimeout(httpParameters, MainActivity.CONNECTION_TIMEOUT_SEC * 2000);

            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.
            HttpConnectionParams.setSoTimeout(httpParameters, MainActivity.SOCKET_TIMEOUT_SEC * 2000);

            for (NameValuePair namePairValue : namePairValues) {
                String value = namePairValue.getValue();
                URL += "&" + namePairValue.getName() + "=" + (value.equals("") ? "*" : value);
            }

            HttpGet request = new HttpGet();
            request.setURI(new URI(URL.replace(" ", "%20")));
            request.setParams(httpParameters);
            response = client.execute(request);
            responseString = convertStreamToString(response.getEntity().getContent());

        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            responseString = null;
        } catch (Exception e) {
            e.printStackTrace();
            responseString = null;
        }

        return responseString;
    }
}