package rs.etf.km123247.amboassist;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Krca on 9/11/13.
 */
class RetreiveAllVisitsTask extends AsyncTask<String, Void, String> {

    private final String baseUrl;
    private final String destinationUrl;
    private final MainActivity activity;
    private ProgressDialog mDialog;

    private ListView view;

    public RetreiveAllVisitsTask(MainActivity activity, ListView textView, String baseUrl, String destinationUrl) {
        this.view = textView;
        this.baseUrl = baseUrl;
        this.destinationUrl = destinationUrl;
        this.activity = activity;
    }

    protected void onPreExecute() {
        try {
            if (this.mDialog == null) {
                this.mDialog = new ProgressDialog(this.view.getContext());
                this.mDialog.setMessage("Loading...");
                this.mDialog.setCancelable(false);
            }
            this.mDialog.show();
        } catch (Exception ignored) {

        }
    }

    protected String doInBackground(String... urls) {
        return HttpClient.SendHttpGet(this.baseUrl + this.destinationUrl);
    }

    protected void onPostExecute(String jsonString) {
        boolean errorOccurred = false;
        try {
            if (jsonString == null) {
                throw new Exception();
            }

            JSONObject jsonResponseObject = new JSONObject(jsonString);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(view.getContext());
            boolean getPatient = sharedPref.getBoolean(SettingsActivity.SETTINGS_GET_PATIENT, false);
            if(!jsonResponseObject.isNull("patient") && getPatient) {
                JSONObject potentialPatientJSON = (JSONObject) jsonResponseObject.get("patient");
                Patient potentialPatient = new Patient((String) potentialPatientJSON.get("name"));
                potentialPatient.setJmbg((String) potentialPatientJSON.get("jmbg"));
                potentialPatient.setSex((Boolean) potentialPatientJSON.get("sex"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date d = dateFormat.parse((String) potentialPatientJSON.get("dob"));
                potentialPatient.setDob(d);
                Patient.foundPatient = potentialPatient;
                activity.alertSinglePatientFound(view.getContext());
            }
            JSONArray jsons = new JSONArray((String) jsonResponseObject.get("visits"));
            List<Visit> visits = new ArrayList<Visit>();
            for (int i = 0; i < jsons.length(); i++) {
                JSONObject object = ((JSONObject) jsons.get(i));

//                2013-09-22T13:57:00Z
                String dateString = (String) object.get("date");
                Date date = null;
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                try {
                    date = formatter.parse(dateString);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                Visit visit = new Visit(date, (String) object.get("diagnose"), (String) object.get("symptoms"));
                visit.setAddress((String) object.get("address"));
                Technician t = new Technician((String)((JSONObject)object.get("technician")).get("name"));
                visit.setTechnician(t);
                Patient p = new Patient((String)((JSONObject)object.get("patient")).get("name"));
                p.setJmbg((String)((JSONObject)object.get("patient")).get("jmbg"));
                p.setSex((Boolean)((JSONObject)object.get("patient")).get("sex"));
                String dob = (String)((JSONObject)object.get("patient")).get("dob");
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date d = dateFormat.parse(dob);
                p.setDob(d);
                visit.setPatient(p);

                visits.add(visit);
            }
            VisitItemAdapter adapter = new VisitItemAdapter(view.getContext(),
                    R.layout.visits_list_item_layout, visits.toArray(new Visit[visits.size()]));

            view.setAdapter(adapter);
        } catch (Exception e) {
            errorOccurred = true;
            if(e.getMessage() != null) {
                Log.i("ERROR", e.getMessage());
            }
        } finally {
            if (errorOccurred) {

                MainActivity.alert(view.getContext(), activity.getString(R.string.connection_fail));
            }

            this.mDialog.dismiss();
        }
    }
}