package rs.etf.km123247.amboassist;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by krca on 9/28/13.
 */
public class VisitActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the user interface layout for this Activity
        setContentView(R.layout.activity_visit);

        Intent i = getIntent();

        final Visit visit = (Visit) i.getSerializableExtra("visit");
        assert visit != null;

        TextView address = (TextView) findViewById(R.id.visit_activity_address);
        address.setText(visit.getAddress());

        TextView diagnose = (TextView) findViewById(R.id.visit_activity_diagnose);
        diagnose.setText(visit.getDiagnose());

        TextView symptoms = (TextView) findViewById(R.id.visit_activity_symptoms);
        symptoms.append(visit.getSymptoms().replace(";", "\n"));

        TextView patient = (TextView) findViewById(R.id.visit_activity_patient);
        patient.append(visit.getPatient().getName());

        TextView jmbg = (TextView) findViewById(R.id.visit_activity_patient_jmbg);
        jmbg.append(visit.getPatient().getJmbg());

        TextView technician = (TextView) findViewById(R.id.visit_activity_technician);
        technician.append(visit.getTechnician().getName());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(visit.getDate());
        TextView date = (TextView) findViewById(R.id.visit_activity_date);
        date.setText(calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR));

        TextView time = (TextView) findViewById(R.id.visit_activity_time);
        time.setText(calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE));

        // Make sure we're running on ICS or higher to use ActionBar APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            // For the main activity, make sure the app icon in the action bar
            // does not behave as a button
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setHomeButtonEnabled(true);
            }
        }


        Button createNew = (Button) findViewById(R.id.visit_create_new);
        createNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("patient", visit.getPatient());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });

    }
}
