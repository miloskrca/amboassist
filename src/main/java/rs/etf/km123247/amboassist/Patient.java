package rs.etf.km123247.amboassist;

import java.util.List;

/**
 * Created by Krca on 9/11/13.
 */
public class Patient extends Person {
    public static List<Patient> patientList;
    public static Patient foundPatient;

    public Patient(String name) {
        super(name);
    }

    public static int getIdFromJmbg(String jmbg) {
        if(Patient.patientList != null) {
            for (Patient patient : Patient.patientList) {
                if(jmbg.equals(patient.getJmbg())) {
                    return patient.getId();
                }
            }
        }
        return -1;
    }

    public static Patient getPatientFromId(Integer patient_id) {
        if(Patient.patientList != null) {
            for (Patient patient : Patient.patientList) {
                if(patient.getId().equals(patient_id))
                    return patient;
            }
        }
        return null;
    }
}
