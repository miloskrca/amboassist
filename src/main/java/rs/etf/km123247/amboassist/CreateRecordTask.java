package rs.etf.km123247.amboassist;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;

import java.util.List;

/**
 * Created by Krca on 9/11/13.
 */
class CreateRecordTask extends AsyncTask<String, Void, String> {

    private final String baseUrl;
    private final String destinationUrl;
    private final List<NameValuePair> nameValuePairs;
    private final LinearLayout layout;
    private ProgressDialog mDialog;

    private Context context;

    public CreateRecordTask(LinearLayout layout, Context context, String baseUrl, String destinationUrl, List<NameValuePair> nameValuePairs) {
        this.context = context;
        this.layout = layout;
        this.baseUrl = baseUrl;
        this.destinationUrl = destinationUrl;
        this.nameValuePairs = nameValuePairs;
    }

    protected void onPreExecute() {
        try {
            if (this.mDialog == null) {
                this.mDialog = new ProgressDialog(this.context);
                this.mDialog.setMessage("Loading...");
                this.mDialog.setCancelable(false);
            }
            this.mDialog.show();
        } catch (Exception ignored) {

        }
    }

    protected String doInBackground(String... urls) {
        return HttpClient.SendHttpPost(this.baseUrl + this.destinationUrl, this.nameValuePairs);
    }

    protected void onPostExecute(String jsonString) {
        boolean errorOccurred = false;
        String code = jsonString;
        try {
            if (jsonString == null) {
                throw new Exception("Server unreachable.");
            }

        } catch (Exception e) {
            errorOccurred = true;
            code = e.getMessage();
            Log.i("ERROR", e.getMessage());
        } finally {

            // Setting Dialog Message
            String msg;
            if(errorOccurred) {
                msg = code;
            } else if (code.equals("201")) {
                msg = "Success";
                for (int i = 0; i < PatientTextWatcher.flags.size(); i++) {
                    PatientTextWatcher.flags.put(PatientTextWatcher.flags.keyAt(i), false);
                }

                for (int i = 0, count = layout.getChildCount(); i < count; ++i) {
                    Object view = layout.getChildAt(i);
                    if (view instanceof EditText) {
                        ((EditText)view).setText("");
                    } else if (view instanceof TableLayout) {
                        int numOfPairs = ((TableLayout)view).getChildCount();
                        for(int j = 1; j < numOfPairs; j++) { // i=1 because of the header
                            if(((TableLayout)view).getChildAt(j) != null) {
                                ((TableLayout)view).removeViewAt(j);
                            }
                        }
                    } else if (view instanceof TextView) {
                        TextView v = (TextView)view;
                        if(v.getId() == R.id.magic_patient_search_status) {
                            v.setBackgroundColor(Color.WHITE);
                            v.setText("");
                        }
                    }
                }

                final TextView status = (TextView) layout.findViewById(R.id.magic_patient_search_status);
                status.setBackgroundColor(Color.WHITE);
                status.setText("");
                for (int i = 0; i < PatientTextWatcher.flags.size(); i++) {
                    PatientTextWatcher.flags.put(PatientTextWatcher.flags.keyAt(i), true);
                }
                Patient.foundPatient = null;
            } else {
                msg = "Unknown error, code: " + code;
            }
            MainActivity.alert(context, msg);
            this.mDialog.dismiss();
        }
    }
}