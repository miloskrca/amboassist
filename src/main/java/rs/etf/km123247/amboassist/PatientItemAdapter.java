package rs.etf.km123247.amboassist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by Krca on 9/11/13.
 */
public class PatientItemAdapter extends ArrayAdapter {
    Context context;
    int layoutResourceId;
    Patient data[] = null;

    public PatientItemAdapter(Context context, int layoutResourceId, Patient[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PatientHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new PatientHolder();
            assert row != null;
            holder.name = (TextView) row.findViewById(R.id.patient_name);
            holder.id = (TextView) row.findViewById(R.id.patient_id);
            holder.jmbg = (TextView) row.findViewById(R.id.patient_jmbg);

            row.setTag(holder);
        } else {
            holder = (PatientHolder) row.getTag();
        }

        Patient patient = data[position];
        holder.name.setText(patient.getName());
        holder.id.setText(patient.getId().toString());
        holder.jmbg.setText(patient.getJmbg().toString());

        return row;
    }

    static class PatientHolder {
        public TextView name;
        public TextView id;
        public TextView jmbg;
    }
}