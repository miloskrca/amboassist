package rs.etf.km123247.amboassist;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Krca on 9/15/13.
*/
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}