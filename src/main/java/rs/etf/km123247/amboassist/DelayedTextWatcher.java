package rs.etf.km123247.amboassist;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by krca on 9/29/13.
 */
public abstract class DelayedTextWatcher implements TextWatcher {

    private long delayTime;
    private WaitTask lastWaitTask;
    private boolean isDelayDone = true;

    public DelayedTextWatcher(long delayTime) {
        super();
        this.delayTime = delayTime;
    }

    @Override
    public void afterTextChanged(Editable s) {
        synchronized (this) {
            if (lastWaitTask != null) {
                lastWaitTask.cancel(true);
            }
            lastWaitTask = new WaitTask();
            lastWaitTask.execute(s);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub
        if(isDelayDone) {
            beforeTextChangedDelayed();
            isDelayDone = false;
        }
    }

    public abstract void beforeTextChangedDelayed();
    public abstract void afterTextChangedDelayed(Editable s);

    private class WaitTask extends AsyncTask<Editable, Void, Editable> {

        @Override
        protected Editable doInBackground(Editable... params) {
            try {
                Thread.sleep(delayTime);
            } catch (InterruptedException e) {
            }
            return params[0];
        }

        @Override
        protected void onPostExecute(Editable result) {
            super.onPostExecute(result);
            afterTextChangedDelayed(result);
            isDelayDone = true;
        }
    }
}
