package rs.etf.km123247.amboassist;

import android.text.Editable;
import android.util.SparseBooleanArray;

/**
 * Created by krca on 10/6/13.
 */
public abstract class PatientTextWatcher extends DelayedTextWatcher {
    public static SparseBooleanArray flags = new SparseBooleanArray(4);
    public static int NAME = 0;
    public static int JMBG = 1;
    public static int SEX = 2;
    public static int DOB = 3;
    public final int FLAG;

    public PatientTextWatcher(long delayTime, int FLAG) {
        super(delayTime);
        this.FLAG = FLAG;
        flags.put(NAME, true);
        flags.put(JMBG, true);
        flags.put(SEX, true);
        flags.put(DOB, true);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(flags.get(FLAG)) {
            super.onTextChanged(s, start, before, count);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if(flags.get(FLAG)) {
            super.afterTextChanged(s);
        }
    }
}
