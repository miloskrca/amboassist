package rs.etf.km123247.amboassist;

/**
 * Created by Krca on 9/11/13.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A dummy fragment representing a section of the app, but that simply
 * displays dummy text.
 */
public class SectionFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    public static final String ARG_SECTION_NUMBER = "section_number";
    public static final int VISIT_FROM_VISIT = 1;

    public SectionFragment() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = null;
        switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
            case 1:
                rootView = inflater.inflate(R.layout.fragment_visits_list, container, false);
                assert rootView != null;

                final Button button = (Button) rootView.findViewById(R.id.visits_list_get_visits_button);
                final ListView visitsList = (ListView) rootView.findViewById(R.id.visits_list);
                final EditText magic = (EditText) rootView.findViewById(R.id.visits_list_magic);
                final Spinner filter = (Spinner) rootView.findViewById(R.id.visits_list_filter);
                final Spinner filterDir = (Spinner) rootView.findViewById(R.id.visits_list_filter_direction);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        visitsList.removeAllViewsInLayout();

                        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(v.getContext());
                        boolean getPatient = sharedPref.getBoolean(SettingsActivity.SETTINGS_GET_PATIENT, false);
                        String url = sharedPref.getString(SettingsActivity.SETTINGS_URL, MainActivity.BASE_URL);
                        String destination = "visits.json";
                        try {
                            if (!magic.getText().toString().equals("")) {
                                destination += "?magic=" + magic.getText().toString() + "&sortBy=" + filter.getSelectedItemPosition();
                            } else {
                                destination += "?sortBy=" + filter.getSelectedItemPosition();
                            }
                            destination += "&sortByDirection=" + filterDir.getSelectedItemPosition();
                            destination += "&getSinglePatient=" + (getPatient ? "1" : "0");
                        } catch (Exception ignored) {
                        }
                        new RetreiveAllVisitsTask(MainActivity.mActivity, visitsList, url, destination).execute();
                    }
                });

                visitsList.setClickable(true);
                visitsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

                        Visit visit = (Visit) visitsList.getItemAtPosition(position);
                        Intent i = new Intent(getActivity(), VisitActivity.class);
                        i.putExtra("visit", visit);
                        getActivity().startActivityForResult(i, VISIT_FROM_VISIT);
                    }
                });

                break;
            case 2:
                rootView = inflater.inflate(R.layout.fragment_visit, container, false);
                assert rootView != null;

                final Context context = rootView.getContext();

                //init technicianList
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
                final String url = sharedPref.getString(SettingsActivity.SETTINGS_URL, MainActivity.BASE_URL);

                final AutoCompleteTextView technicianView = (AutoCompleteTextView) rootView.findViewById(R.id.visit_technician);

                if (Technician.technicianList == null) {
                    Technician.technicianList = new ArrayList<Technician>();
                    new RetreiveTechnitiansTask(rootView.getContext(), url, "technicians.json", technicianView).execute();
                }

                final EditText patientView = (EditText) rootView.findViewById(R.id.patient_name);
                patientView.addTextChangedListener(new ExistanceHelper(patientView, "patients"));

                Button addSymptom = (Button) rootView.findViewById(R.id.visit_add_symptom);
                final View finalRootView = rootView;
                addSymptom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final LinearLayout layout = (LinearLayout) finalRootView.findViewById(R.id.part_symptoms_pair_table);
                        final TableRow pairLayout = (TableRow) inflater.inflate(R.layout.part_symptom_pair_layout, container, false);

                        if (pairLayout != null) {

                            AutoCompleteTextView locationView = (AutoCompleteTextView) pairLayout.findViewById(R.id.dynamic_location);
                            String[] locations = getResources().getStringArray(R.array.location_array);
                            ArrayAdapter<String> adapterLocation =
                                    new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, locations);
                            locationView.setAdapter(adapterLocation);


                            AutoCompleteTextView symptomsView = (AutoCompleteTextView) pairLayout.findViewById(R.id.dynamic_symptom);
                            String[] symptoms = getResources().getStringArray(R.array.symptoms_array);
                            ArrayAdapter<String> adapterSymptoms =
                                    new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, symptoms);
                            symptomsView.setAdapter(adapterSymptoms);


                            Button removeSymptomBtn = (Button) pairLayout.findViewById(R.id.remove_symptom);
                            removeSymptomBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    layout.removeView(pairLayout);
                                }
                            });

                            layout.addView(pairLayout);
                        }

                    }
                });

                final LinearLayout linLayout = (LinearLayout) rootView.findViewById(R.id.visit_layout);
                final Button saveVisit = (Button) rootView.findViewById(R.id.visit_save);
                final TableLayout tableLocationSymptomPair = (TableLayout) rootView.findViewById(R.id.part_symptoms_pair_table);
                final EditText addressView = (EditText) rootView.findViewById(R.id.visit_address);
                final EditText diagnoseView = (EditText) rootView.findViewById(R.id.visit_diagnose);
                final DatePicker dateView = (DatePicker) rootView.findViewById(R.id.visit_date);
                final TimePicker timeView = (TimePicker) rootView.findViewById(R.id.visit_time);

                final EditText nameView = (EditText) rootView.findViewById(R.id.patient_name);
                final EditText jmbgView = (EditText) rootView.findViewById(R.id.patient_jmbg);
                final DatePicker dobView = (DatePicker) rootView.findViewById(R.id.patient_dob);
                final RadioButton maleView = (RadioButton) rootView.findViewById(R.id.patient_male);
                final RadioButton femaleView = (RadioButton) rootView.findViewById(R.id.patient_female);
                final RadioGroup radioGroup = (RadioGroup) rootView.findViewById(R.id.patient_gender);
                final Button clear = (Button) rootView.findViewById(R.id.visit_clear_patient);
                final Button acc = (Button) rootView.findViewById(R.id.visit_accept_patient);
                final TextView status = (TextView) rootView.findViewById(R.id.magic_patient_search_status);

                clear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acc.setEnabled(false);
                        for (int i = 0; i < PatientTextWatcher.flags.size(); i++) {
                            PatientTextWatcher.flags.put(PatientTextWatcher.flags.keyAt(i), false);
                        }
                        nameView.setText("");
                        jmbgView.setText("");
                        femaleView.setChecked(false);
                        maleView.setChecked(true);
                        status.setBackgroundColor(Color.WHITE);
                        status.setText("");
                        for (int i = 0; i < PatientTextWatcher.flags.size(); i++) {
                            PatientTextWatcher.flags.put(PatientTextWatcher.flags.keyAt(i), true);
                        }
                    }
                });

                this.setTextWatcher(context, url, nameView, rootView, PatientTextWatcher.NAME);
                this.setTextWatcher(context, url, jmbgView, rootView, PatientTextWatcher.JMBG);
                this.setRadioGroupWatcher(context, url, radioGroup, rootView);

                saveVisit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // save visit
                        int numOfPairs = tableLocationSymptomPair.getChildCount();
                        StringBuilder symptomsString = new StringBuilder();
                        for (int i = 1; i < numOfPairs; i++) { // i=1 because of the header
                            TableRow row = (TableRow) tableLocationSymptomPair.getChildAt(i);
                            if (row != null) {
                                String location = ((AutoCompleteTextView) row.findViewById(R.id.dynamic_location)).getText().toString();
                                String symptom = ((AutoCompleteTextView) row.findViewById(R.id.dynamic_symptom)).getText().toString();
                                if (!location.equals("") && !symptom.equals("")) {
                                    symptomsString.append(location).append(":").append(symptom);
                                }
                            }
                            symptomsString.append(";");
                        }
                        String symptoms = symptomsString.toString();

                        int tId = Technician.getIdFromName(technicianView.getText().toString());
                        int pId = Patient.getIdFromJmbg(jmbgView.getText().toString());
                        int year = dateView.getYear();
                        int month = dateView.getMonth();
                        int day = dateView.getDayOfMonth();
                        int hour = timeView.getCurrentHour();
                        int minute = timeView.getCurrentMinute();
                        String d = diagnoseView.getText().toString();
                        String address = addressView.getText().toString();

                        if (tId == -1
//                                || pId == -1
                                || d.equals("")
                                || address.equals("")
                                || symptoms.equals("")) {
                            MainActivity.alert(v.getContext(), "Fill in all fields!");
                            return;
                        }

                        String pName = nameView.getText().toString();
                        String pJmbg = jmbgView.getText().toString();
                        int pYear = dobView.getYear();
                        int pMonth = dobView.getMonth();
                        int pDay = dobView.getDayOfMonth();

                        if (pName.equals("") || pJmbg.equals("")) {
                            MainActivity.alert(v.getContext(), "Fill in all fields!!");
                            return;
                        }

                        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(v.getContext());
                        String url = sharedPref.getString(SettingsActivity.SETTINGS_URL, MainActivity.BASE_URL);

                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(15);

                        nameValuePairs.add(new BasicNameValuePair("patient[name]", pName));
                        nameValuePairs.add(new BasicNameValuePair("patient[jmbg]", pJmbg));
                        nameValuePairs.add(new BasicNameValuePair("patient[dob(1i)]", pYear + ""));
                        nameValuePairs.add(new BasicNameValuePair("patient[dob(2i)]", pMonth + ""));
                        nameValuePairs.add(new BasicNameValuePair("patient[dob(3i)]", pDay + ""));
                        nameValuePairs.add(new BasicNameValuePair("patient[sex]", maleView.isChecked() ? "true" : "false"));

                        nameValuePairs.add(new BasicNameValuePair("visit[symptoms]", symptoms));
                        nameValuePairs.add(new BasicNameValuePair("visit[address]", address));
                        nameValuePairs.add(new BasicNameValuePair("visit[diagnose]", d));
                        nameValuePairs.add(new BasicNameValuePair("visit[date(1i)]", year + ""));
                        nameValuePairs.add(new BasicNameValuePair("visit[date(2i)]", month + ""));
                        nameValuePairs.add(new BasicNameValuePair("visit[date(3i)]", day + ""));
                        nameValuePairs.add(new BasicNameValuePair("visit[date(4i)]", hour + ""));
                        nameValuePairs.add(new BasicNameValuePair("visit[date(5i)]", minute + ""));
                        nameValuePairs.add(new BasicNameValuePair("visit[technician_id]", tId + ""));
                        nameValuePairs.add(new BasicNameValuePair("visit[patient_id]", pId + ""));

                        new CreateRecordTask(linLayout, v.getContext(), url, "visits.json?found=0", nameValuePairs).execute();
                    }
                });
                break;
        }
        return rootView;
    }

    private void setRadioGroupWatcher(final Context context, final String url, RadioGroup radioGroup, View rootView) {
        final EditText nameView = (EditText) rootView.findViewById(R.id.patient_name);
        final EditText jmbgView = (EditText) rootView.findViewById(R.id.patient_jmbg);
        final DatePicker dobView = (DatePicker) rootView.findViewById(R.id.patient_dob);
        final RadioButton maleView = (RadioButton) rootView.findViewById(R.id.patient_male);
        final RadioButton femaleView = (RadioButton) rootView.findViewById(R.id.patient_female);
        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.visit_progress_bar);
        final TextView status = (TextView) rootView.findViewById(R.id.magic_patient_search_status);

        final Button acc = (Button) rootView.findViewById(R.id.visit_accept_patient);
        acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Patient.foundPatient != null) {
                    if (MainActivity.mActivity != null) {
                        MainActivity.mActivity.alertSinglePatientFound(v.getContext());
                    }
                }
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (!PatientTextWatcher.flags.get(PatientTextWatcher.SEX)) {
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                new RetreivePatientTask(context, url + "patients.json?magic=1", nameView.getText().toString(), jmbgView.getText().toString(), maleView.isChecked()) {
                    @Override
                    public void onFinished(int count, JSONObject object) {
                        super.onFinished(count, object);
                        Patient.foundPatient = null;
                        acc.setEnabled(false);
                        if (count == 1) {
                            acc.setEnabled(true);
                            // turn off watching
                            PatientTextWatcher.flags.put(PatientTextWatcher.SEX, false);
                            try {
                                int id = (Integer) object.get("id");
                                String jmbg = (String) object.get("jmbg");
                                String name = (String) object.get("name");
                                boolean sex = (Boolean) object.get("sex");
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                Date d = dateFormat.parse((String) object.get("dob"));

                                Patient foundPatient = new Patient(name);
                                foundPatient.setJmbg(jmbg);
                                foundPatient.setSex(sex);
                                foundPatient.setDob(d);
                                Patient.foundPatient = foundPatient;

                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } finally {
                                // turn on watching
                                PatientTextWatcher.flags.put(PatientTextWatcher.SEX, true);
                            }
                            status.setBackgroundColor(Color.GREEN);
                            status.setTextColor(Color.BLACK);
                            status.setText(count + " " + getResources().getString(R.string.result_found));
                        } else if (count > 1) {
                            status.setBackgroundColor(Color.YELLOW);
                            status.setTextColor(Color.BLACK);
                            status.setText(count + " " + getResources().getString(R.string.results_found));
                        } else {
                            status.setBackgroundColor(Color.RED);
                            status.setTextColor(Color.WHITE);
                            status.setText(count + " " + getResources().getString(R.string.results_found));
                        }

                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }.execute();
            }
        });
    }


    private void setTextWatcher(final Context context, final String url, final EditText view, View rootView, int FLAG) {
        final EditText nameView = (EditText) rootView.findViewById(R.id.patient_name);
        final EditText jmbgView = (EditText) rootView.findViewById(R.id.patient_jmbg);
        final DatePicker dobView = (DatePicker) rootView.findViewById(R.id.patient_dob);
        final RadioButton maleView = (RadioButton) rootView.findViewById(R.id.patient_male);
        final RadioButton femaleView = (RadioButton) rootView.findViewById(R.id.patient_female);
        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.visit_progress_bar);
        final TextView status = (TextView) rootView.findViewById(R.id.magic_patient_search_status);

        final Button acc = (Button) rootView.findViewById(R.id.visit_accept_patient);
        acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Patient.foundPatient != null) {
                    if (MainActivity.mActivity != null) {
                        MainActivity.mActivity.alertSinglePatientFound(v.getContext());
                    }
                }
            }
        });

        view.addTextChangedListener(new PatientTextWatcher(1000, FLAG) {

            @Override
            public void beforeTextChangedDelayed() {
            }

            @Override
            public void afterTextChangedDelayed(Editable s) {
                if (nameView.getText().toString().equals("") && jmbgView.getText().toString().equals("")) {
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                new RetreivePatientTask(context, url + "patients.json?magic=1", nameView.getText().toString(), jmbgView.getText().toString(), maleView.isChecked()) {
                    @Override
                    public void onFinished(int count, JSONObject object) {
                        super.onFinished(count, object);
                        Patient.foundPatient = null;
                        acc.setEnabled(false);
                        if (count == 1) {
                            acc.setEnabled(true);
                            // turn off watching
                            PatientTextWatcher.flags.put(FLAG, false);
                            try {
                                int id = (Integer) object.get("id");
                                String jmbg = (String) object.get("jmbg");
                                String name = (String) object.get("name");
                                boolean sex = (Boolean) object.get("sex");
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                Date d = dateFormat.parse((String) object.get("dob"));

                                Patient foundPatient = new Patient(name);
                                foundPatient.setJmbg(jmbg);
                                foundPatient.setSex(sex);
                                foundPatient.setDob(d);
                                Patient.foundPatient = foundPatient;

                            } catch (ParseException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } finally {
                                // turn on watching
                                PatientTextWatcher.flags.put(FLAG, true);
                            }
                            status.setBackgroundColor(Color.GREEN);
                            status.setTextColor(Color.BLACK);
                            status.setText(count + " " + getResources().getString(R.string.result_found));
                        } else if (count > 1) {
                            status.setBackgroundColor(Color.YELLOW);
                            status.setTextColor(Color.BLACK);
                            status.setText(count + " " + getResources().getString(R.string.results_found));
                        } else {
                            status.setBackgroundColor(Color.RED);
                            status.setTextColor(Color.WHITE);
                            status.setText(count + " " + getResources().getString(R.string.results_found));
                        }

                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }.execute();
            }
        });
    }
}