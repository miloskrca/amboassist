package rs.etf.km123247.amboassist;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by Krca on 9/11/13.
 */
public class PatientActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the user interface layout for this Activity
        setContentView(R.layout.activity_patient);

        Intent i = getIntent();
        boolean isEditable = "true".equals(i.getSerializableExtra("isEditable"));

        if(isEditable) {
            Patient patient = (Patient) i.getSerializableExtra("patient");

            TextView id = (TextView) findViewById(R.id.patient_id);
            id.setText(patient.getId().toString());

            EditText name = (EditText) findViewById(R.id.patient_name);
            name.setText(patient.getName());

            EditText jmbg = (EditText) findViewById(R.id.patient_jmbg);
            jmbg.setText(patient.getJmbg());

            RadioButton male = (RadioButton) findViewById(R.id.patient_male);
            RadioButton female = (RadioButton) findViewById(R.id.patient_female);
            male.setChecked(patient.getSex());
            female.setChecked(!patient.getSex());

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(patient.getDob());
            DatePicker dob = (DatePicker) findViewById(R.id.patient_dob);
            dob.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        }

        // Make sure we're running on ICS or higher to use ActionBar APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            // For the main activity, make sure the app icon in the action bar
            // does not behave as a button
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setHomeButtonEnabled(true);
            }
        }
    }
}
