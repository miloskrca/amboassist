package rs.etf.km123247.amboassist;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Krca on 9/11/13.
 */
public class Visit implements Serializable {
    private Date date;
    private String diagnose;
    private String symptoms;
    private Technician technician;
    private Patient patient;
    private String address;

    public Visit(Date date, String diagnose, String symptoms) {
        this.date = date;
        this.diagnose = diagnose;
        this.symptoms = symptoms;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public Technician getTechnician() {
        return technician;
    }

    public void setTechnician(Technician technician) {
        this.technician = technician;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
