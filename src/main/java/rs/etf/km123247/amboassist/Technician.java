package rs.etf.km123247.amboassist;

import java.util.List;

/**
 * Created by Krca on 9/11/13.
 */
public class Technician extends Person {

    public static List<Technician> technicianList;

    public Technician(String name) {
        super(name);
    }

    public static int getIdFromName(String name) {
        if(Technician.technicianList != null) {
            for (Technician technician : Technician.technicianList) {
                if(name.equals(technician.getName()))
                    return technician.getId();
            }
        }

        return -1;
    }

    public static Technician getTechnicianFromId(Integer technician_id) {
        if(Technician.technicianList != null) {
            for (Technician technician : Technician.technicianList) {
                if(technician.getId().equals(technician_id))
                    return technician;
            }
        }
        return null;
    }
}
