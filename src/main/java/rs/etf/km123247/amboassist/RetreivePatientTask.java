package rs.etf.km123247.amboassist;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Krca on 9/11/13.
 */

class RetreivePatientTask extends AsyncTask<String, Void, String> {


    private final List<NameValuePair> namePairValues;
    private final Context context;
    private final String URL;

    public RetreivePatientTask(Context context, String URL, String name, String jmbg, boolean sex/*, Date dob*/) {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
        nameValuePairs.add(new BasicNameValuePair("patient[name]", name));
        nameValuePairs.add(new BasicNameValuePair("patient[jmbg]", jmbg));
        nameValuePairs.add(new BasicNameValuePair("patient[sex]", sex ? "true" : "false"));
//        if(dob != null) {
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(Patient.foundPatient.getDob());
//
//            nameValuePairs.add(new BasicNameValuePair("patient[dob(1i)]", calendar.get(Calendar.YEAR) + ""));
//            nameValuePairs.add(new BasicNameValuePair("patient[dob(2i)]", calendar.get(Calendar.MONTH) + ""));
//            nameValuePairs.add(new BasicNameValuePair("patient[dob(3i)]", calendar.get(Calendar.DAY_OF_MONTH) + ""));
//        }
        this.namePairValues = nameValuePairs;
        this.context = context;
        this.URL = URL;
    }

    protected void onPreExecute() {

    }

    protected String doInBackground(String... urls) {
        return HttpClient.SendHttpGetPatient(URL, namePairValues);
    }

    protected void onPostExecute(String jsonString) {
        boolean errorOccurred = false;
        JSONObject object = null;
        int count = 0;
        try {
            if (jsonString == null) {
                throw new Exception("Connection timeout.");
            }

            JSONArray jsons = new JSONArray(jsonString);
            count = jsons.length();
            if(jsons.length() == 1) {
                object = ((JSONObject) jsons.get(0));
            }
        } catch (Exception e) {
            errorOccurred = true;
            if (e.getMessage() != null) {
                Log.i("ERROR", e.getMessage());
            }
        } finally {
            if (errorOccurred) {
                MainActivity.alert(context, context.getString(R.string.connection_fail));
            } else {
                onFinished(count, object);
            }
        }
    }

    public void onFinished(int count, JSONObject object){};
}