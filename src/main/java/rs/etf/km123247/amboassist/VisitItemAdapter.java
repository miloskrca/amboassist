package rs.etf.km123247.amboassist;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by Krca on 9/11/13.
 */
public class VisitItemAdapter extends ArrayAdapter {
    Context context;
    int layoutResourceId;
    Visit[] data = null;

    public VisitItemAdapter(Context context, int layoutResourceId, Visit[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        VisitHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new VisitHolder();
            assert row != null;
            holder.date = (TextView) row.findViewById(R.id.visit_single_date);
            holder.technician = (TextView) row.findViewById(R.id.visit_single_technician_name);
            holder.patientName = (TextView) row.findViewById(R.id.visit_single_patient_name);
            holder.patientJMBG = (TextView) row.findViewById(R.id.visit_single_patient_jmbg);

            row.setTag(holder);
        } else {
            holder = (VisitHolder) row.getTag();
        }

        Visit visit = data[position];

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(visit.getDate());

        try {
        holder.date.setText(calendar.get(Calendar.YEAR)
                +"-"+(calendar.get(Calendar.MONTH)+1)
                +"-"+calendar.get(Calendar.DAY_OF_MONTH)
                +" "+calendar.get(Calendar.HOUR_OF_DAY)
                +":"+calendar.get(Calendar.MINUTE)
        );
            holder.technician.setText(visit.getTechnician().getName());
            holder.patientName.setText(visit.getPatient().getName());
            holder.patientJMBG.setText(visit.getPatient().getJmbg());
        } catch (Exception e) {
            Log.i("ERROR", e.getMessage());
        }
        return row;
    }

    static class VisitHolder {
        public TextView date;
        public TextView technician;
        public TextView patientName;
        public TextView patientJMBG;
    }
}