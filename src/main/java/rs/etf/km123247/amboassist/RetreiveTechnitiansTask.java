package rs.etf.km123247.amboassist;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Krca on 9/11/13.
 */
class RetreiveTechnitiansTask extends AsyncTask<String, Void, String> {

    private final String baseUrl;
    private final String destinationUrl;
    private final Context context;
    private final AutoCompleteTextView technicianView;
    private ProgressDialog mDialog;

    public RetreiveTechnitiansTask(Context context, String baseUrl, String destinationUrl, AutoCompleteTextView technicianView) {
        this.context = context;
        this.baseUrl = baseUrl;
        this.destinationUrl = destinationUrl;
        this.technicianView = technicianView;
    }

    protected void onPreExecute() {
        try {
            this.mDialog = new ProgressDialog(this.context);
            this.mDialog.setMessage("Loading...");
            this.mDialog.setCancelable(false);
            this.mDialog.show();
        } catch (Exception ignored) {

        }
    }

    protected String doInBackground(String... urls) {
        return HttpClient.SendHttpGet(this.baseUrl + this.destinationUrl);
    }

    protected void onPostExecute(String jsonString) {
        boolean errorOccurred = false;
        try {
            if (jsonString == null) {
                throw new Exception("Error");
            }

            JSONArray jsons;
            jsons = new JSONArray(jsonString);
            if(Technician.technicianList != null) {
                Technician.technicianList.clear();
            } else {
                Technician.technicianList = new ArrayList<Technician>();
            }
            for (int i = 0; i < jsons.length(); i++) {
                JSONObject object = ((JSONObject) jsons.get(i));

                Technician technician = new Technician((String) object.get("name"));
                technician.setId((Integer) object.get("id"));
                if(Technician.technicianList != null) {
                    Technician.technicianList.add(technician);
                }
            }

//            //fill in technicians
            int technicianCount = Technician.technicianList.size();
            Technician[] technicians = Technician.technicianList.toArray(new Technician[technicianCount]);
            String[] technicianNames = new String[technicianCount];
            for(int i = 0; i < technicianCount; i++) {
                technicianNames[i] = technicians[i].getName();
            }
            ArrayAdapter<String> adapterTechnicians =
                    new ArrayAdapter<String>(context.getApplicationContext(), R.layout.custom_dropdown, technicianNames);
            this.technicianView.setAdapter(adapterTechnicians);

        } catch (Exception e) {
            errorOccurred = true;
            if(e.getMessage() != null) {
                Log.i("ERROR", e.getMessage());
            }
        } finally {
            if (errorOccurred) {
                MainActivity.alert(context, context.getString(R.string.connection_fail));
            }

            this.mDialog.dismiss();
        }
    }
}